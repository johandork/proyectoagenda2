import { Component, OnInit } from '@angular/core';
import { Objetivo } from '../clases/objetivo'
import { ServicioObjetivosService } from '../servicios/servicio-objetivos.service';
import { ServicioUsuarioService } from '../servicios/servicio-usuario.service';
import { Usuario } from '../clases/usuario';
declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/login', title: 'Inicio de Sesion',  icon:'person', class: '' },
    { path: '/registro', title: 'Registro',  icon:'library_books', class: '' },
    { path: '/calendario', title: 'Calendario',  icon:'content_paste', class: '' },
    { path: '/actividadcrear', title: 'Nueva Actividad',  icon:'unarchive', class: '' },
    { path: '/editar', title: 'Editar Actividad', icon: 'dashboard', class: ''},
    { path: '/rendimiento', title: 'Rendimiento', icon: 'assessment', class:'' },
    { path: '/pagprincipal', title: 'Pagina Principal', icon: 'date_range', class:'' },
    { path: '/objetivos', title: 'Objetivos', icon: 'date_range', class:'' },
    { path: '/perfil', title: 'Perfil', icon: 'date_range', class:'' }
];
@Component({
  selector: 'app-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.css']
})



export class ObjetivosComponent implements OnInit {
  menuItems: any[];
  miObjetivo:Objetivo;
  listaObjetivos:Objetivo[];
  botonSummit:string;
  usuario:Usuario;
  constructor(private servO:ServicioObjetivosService, private servU:ServicioUsuarioService) { 
    this.miObjetivo=new Objetivo();
    this.usuario=servU.getUsuario();
    this.listarMisObjetivos();
    this.botonSummit='Nuevo';
    }

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  
  listarMisObjetivos(){
    this.servO.listarObjetivosget(this.usuario.id).subscribe(
      misobj =>{
        this.listaObjetivos=misobj;
    },
    err => console.log(err)),
    ()=>console.log('termino peticion de lista')
  }
  summit(){
    
    /*if(this.miObjetivo.id_objetivo===0){
      console.log('mi posObje: ',this.miPosObjetivo)
      //this.listaObjetivos.push(this.miObjetivo);
      )*/
      this.miObjetivo.id_estudiante=this.usuario.id;// se debe cambiar a manera dinamica
      this.miObjetivo.id_estudiante=1
      console.log('El id del usuario es: ',this.miObjetivo.id_estudiante)
      console.log('El id del objetivo es: ',this.miObjetivo.id_objetivo)
    if(this.miObjetivo.id_objetivo==undefined){
      console.log('entro1', this.miObjetivo)
      
      this.servO.agregarObjetivo(this.miObjetivo).subscribe(
        data =>console.log('response ',data),
        err => console.error(err),
        ()=>console.log('se agrego un objetivo'))

        this.listaObjetivos.push(this.miObjetivo);
      }else{
        console.log('entro2', this.miObjetivo)
        
        this.servO.actualizarObjetivo(this.miObjetivo).subscribe(
          data =>console.log('response actualizar ',data),
          err => console.error(err),
          ()=>console.log('se actualizo un objetivo'))
        }
        this.miObjetivo=new Objetivo();
        this.botonSummit='Nuevo';


  }
  seleccionar(obj:Objetivo){
    this.miObjetivo=obj;
    this.botonSummit='Actualizar';
    console.log('Se selecciono el ',this.miObjetivo);

  }
  eliminar(obj:Objetivo){
    if (confirm('Esta seguro de eliminar el objetivo '+obj.nom_objetivo)){
      this.servO.eliminarObjetivo(obj).subscribe(
        data => console.log('se elimino ',data),
        err => console.log('el error es: ',err),
        () => console.log('se elimino ', obj)
      )
      this.miObjetivo=obj
    this.listaObjetivos =this.listaObjetivos.filter(x => x != this.miObjetivo)
    this.miObjetivo=new Objetivo()
    }
  }

}
