import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { ServicioActividadService } from './servicios/servicio-actividad.service';
import { ServicioUsuarioService } from './servicios/servicio-usuario.service';
import { ServicioActividadesextraService } from './servicios/servicio-actividadesextra.service';
import { ServicioMateriaService } from './servicios/servicio-materia.service';
import { ServicioCompanerosService } from './servicios/servicio-companeros.service';
import { ServicioPerfilcomponenteService} from './servicios/servicio-perfilcomponente.service';
import { ServicioObjetivosService } from './servicios/servicio-objetivos.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    RouterModule,
    AppRoutingModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
  ],
  providers: [
    ServicioActividadService,
    ServicioUsuarioService,
    ServicioPerfilcomponenteService,    
    ServicioObjetivosService,
    ServicioMateriaService,
    ServicioCompanerosService,
    ServicioActividadesextraService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
