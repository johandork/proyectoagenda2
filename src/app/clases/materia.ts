import { Dia } from "./dia";

export class Materia{
    id_carrera: number;
    id_materia: number;
    nom_materia: string;
    id_dia: number;
    hora_inicio: number;
    hora_fin: number;
    id_estudiante: number;
}