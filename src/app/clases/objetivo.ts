export class Objetivo{
    id_objetivo:number;
    nom_objetivo:string;
    des_objetivo:string;
    fecha_finalizacion:Date;
    id_estudiante:number;
}