export class Actividad{
    id_actividad: number;
    hora_actividad:string;
    dia_actividad:string;
    nom_actividad:string;
    des_actividad:string;
    estado_actividad:boolean;
    id_prioridad:number;
    id_estudiante:number;
}