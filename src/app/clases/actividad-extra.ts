import { Dia } from "./dia";

export class ActividadesExtra{
    id_rutina: number;
    nom_rutina: string;
    id_dia: number;
    hora_inicio: number;
    hora_fin: number;
    id_estudiante: number;
}