import { Component, OnInit } from '@angular/core';
import { Materia } from '../clases/materia';
import { ActividadesExtra } from '../clases/actividad-extra';
import { Companero } from '../clases/companero';
import { Perfil } from '../clases/perfil';
import { ServicioActividadesextraService} from '../servicios/servicio-actividadesextra.service'
import { ServicioCompanerosService} from '../servicios/servicio-companeros.service'
import { ServicioMateriaService} from '../servicios/servicio-materia.service'
import { ServicioPerfilcomponenteService } from '../servicios/servicio-perfilcomponente.service';
import { Dia } from '../clases/dia';


declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
class diaSemana {
  value: number;
  viewValue: string;
}
class hora {
  value: number;
  viewValue: string;  
}

class nombreMateria {
  value: string;
  viewValue: string;  
}
export const ROUTES: RouteInfo[] = [
    { path: '/login', title: 'Inicio de Sesion',  icon:'person', class: '' },
    { path: '/registro', title: 'Registro',  icon:'library_books', class: '' },
    { path: '/calendario', title: 'Calendario',  icon:'content_paste', class: '' },
    { path: '/actividadcrear', title: 'Nueva Actividad',  icon:'unarchive', class: '' },
    { path: '/editar', title: 'Editar Actividad', icon: 'dashboard', class: ''},
    { path: '/rendimiento', title: 'Rendimiento', icon: 'assessment', class:'' },
    { path: '/pagprincipal', title: 'Pagina Principal', icon: 'date_range', class:'' },
    { path: '/objetivos', title: 'Objetivos', icon: 'date_range', class:'' },
    { path: '/perfil', title: 'Perfil', icon: 'date_range', class:'' }
];


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  menuItems: any[];
  habilitar: Boolean;
  EditarPerfil: String;
  Materias: Materia[]
  ActividadesExtra:ActividadesExtra[]
  perfil:Perfil;
  Companeros:Companero[];
  mimateria:Materia
  miactividade: ActividadesExtra
  dias:Dia[]
  dia:Dia

  nombreMaterias: nombreMateria[] = [
    {value: '0', viewValue: 'Antenas'},
    {value: '1', viewValue: 'Liderazgo'},
    {value: '2', viewValue: 'Big Data'},
    {value: '3', viewValue: 'Cloud'},
    {value: '4', viewValue: 'Telecomunicaciones 2'},
    {value: '5', viewValue: 'Direccion y gestion de proyectos'}];
  dias_semana: diaSemana[] = [
    {value: 0, viewValue: 'Lunes'},
    {value: 1, viewValue: 'Martes'},
    {value: 2, viewValue: 'Miercoles'},
    {value: 3, viewValue: 'Jueves'},
    {value: 4, viewValue: 'Viernes'},
    {value: 5, viewValue: 'Sábado'},
    {value: 6, viewValue: 'Domingo'}
  ];
  horas: hora []=[
  
    {value: 0, viewValue: '12 am'},
    {value: 1, viewValue: '1 am'},
    {value: 2, viewValue: '2 am'},
    {value: 3, viewValue: '3 am'},
    {value: 4, viewValue: '4 am'},
    {value: 5, viewValue: '5 am'},
    {value: 6, viewValue: '6 am'},
    {value: 7, viewValue: '7 am'},
    {value: 8, viewValue: '8 am'},
    {value: 9, viewValue: '9 am'},
    {value: 10, viewValue: '10 am'},
    {value: 11, viewValue: '11 am'},
    {value: 12, viewValue: '12 pm'},
    {value: 13, viewValue: '1 pm'},
    {value: 14, viewValue: '1 pm'},
    {value: 15, viewValue: '1 pm'},
    {value: 16, viewValue: '1 pm'},
    {value: 17, viewValue: '1 pm'},
    {value: 18, viewValue: '1 pm'},
    {value: 19, viewValue: '1 pm'},
    {value: 20, viewValue: '1 pm'},
    {value: 21, viewValue: '1 pm'},
    {value: 22, viewValue: '1 pm'},
    {value: 23, viewValue: '1 pm'}
  ];
  
  
  constructor(private ServicioActividadesextra: ServicioActividadesextraService ,private ServicioMateria: ServicioMateriaService, private ServicioPerfilComponente: ServicioPerfilcomponenteService) { 
    this.dia= new Dia();
    this.dias=[];
    this.perfil=
      { 
        id:null,
        first_name:"",
        last_name:"",
        email:"",
        carr_estudiante:"",
        username:"",
        password:"",
      }

    this.mimateria=new Materia();
    this.miactividade=new ActividadesExtra();
    this.habilitar=true;
    this.EditarPerfil="Editar Perfil";
    this.Materias=[];
    this.ActividadesExtra= [];
    this.Companeros=[{Nombre: "nombre", Fecha: "1/3/20"}];
    this.listarActividadExtra();
    this.listarMaterias();
   
  }

  ngOnInit(): void {
    /*this.ServicioActividadesextra.getActividadesExtra().subscribe(
      data => { this.ActividadesExtra=data
      },
      err => console.error(err)
      )
    this.ServicioCompaneros.getCompaneros().subscribe(
      data => { this.Companeros=data
      },
      err => console.error(err)          
      )
    this.ServicioMaterias.getMaterias().subscribe(
      data => { this.Materias=data
      },
      err => console.error(err)          
      )*/
      /*this.ServicioPerfilComponente.getPerfil().subscribe(
        data => {this.perfil=data
        },
        err => console.error(err)
        )*/
  }
      
  listarMaterias(){
    this.ServicioMateria.getlistarMaterias(1).subscribe(
      mismate =>{
        this.Materias=mismate;
        console.log(mismate)
    },
    err => console.log('Error 1 ',err)),
    ()=>console.log('termino peticion de lista')
  }

  listarActividadExtra(){
    this.ServicioActividadesextra.getlistarActividadExtra(1).subscribe(
      misacte =>{
        this.ActividadesExtra=misacte;
        console.log(misacte)
    },
    err => console.log('Error 2 ',err)),
    ()=>console.log('termino peticion de lista')
  }

  EditarPerfilFunc(){
    this.EditarPerfil=this.EditarPerfil=== "Guardar" ? 'Editar Perfil' : 'Guardar'; 
    this.habilitar=this.habilitar=== true ? false: true;
    console.log("PUT")
    console.log(this.perfil)
    this.ServicioPerfilComponente.putPerfil(this.perfil,2).subscribe()
    console.log(this.perfil)
    //this.estadoCuadro=this.estadoCuadro === 'estado1' ? 'estado2' : 'estado1';
  }

  EliminarM(mate:Materia){
    if (confirm('Esta seguro de eliminar la materia '+mate.id_materia+ '?')){
        this.ServicioMateria.eliminarMaterias(mate).subscribe(
        data => console.log('se elimino ',data),
        err => console.log('el error es: ',err),
        () => console.log('se elimino ', mate)
      ) 
      this.mimateria=mate
      this.Materias =this.Materias.filter(x => x != this.mimateria)
      this.mimateria=new Materia()
    }
  }
  
  EliminarA(acte:ActividadesExtra){
    if (confirm('Esta seguro de eliminar la actividadextra '+acte.nom_rutina+ '?')){
      this.ServicioActividadesextra.eliminarActividadExtra(acte).subscribe(
      data => console.log('se elimino ',data),
      err => console.log('el error es: ',err),
      () => console.log('se elimino ', acte)
    ) 
    this.miactividade=acte
    this.ActividadesExtra =this.ActividadesExtra.filter(x => x != this.miactividade)
    this.miactividade=new ActividadesExtra()
  }
  }  /*crearPerfil(){
    this.ServicioPerfil.getPerfil()
    console.log('Se creo la perfil '+this.perfil.first_name)
    //this.ServicioPerfil.setActividad(this.actividad)
  }*/

  setNombreMateri(nombre:string){
    this.mimateria.nom_materia=nombre;
  }
  setDiaMateri(nombre:number){
    this.mimateria.id_dia=nombre;
  }
  setFInicioMateri(nombre:number){
    this.mimateria.hora_inicio=nombre;
  }
  setFFinMateri(nombre:number){
    this.mimateria.hora_fin=nombre;
  }
  tomardatosmateria(){
    console.log('materia a enviar: ',this.mimateria);
    alert('materia creada '+this.mimateria.nom_materia+'-'+this.mimateria.hora_inicio);
    this.ServicioMateria.agregarMateria(this.mimateria).subscribe(
      data => console.log('se agrego ',data),
      err => console.log('error agregar materia: ',err),
      ()=> console.log('Se agrego ')
      )
      this.listarMaterias();
  }
  tomardatosactividad(){
    console.log('actividad a enviar: ',this.miactividade)
    alert('actvidad creada '+this.miactividade.nom_rutina)
    this.ServicioActividadesextra.agregarActividadE(this.miactividade).subscribe(
      data => console.log('se agrego ',data),
      err => console.log('error agregar actividad: ',err),
      ()=> console.log('Se agrego actividad')
    )
    this.listarMaterias();
  }
  setNombreAct(nombre:string){
    this.miactividade.nom_rutina=nombre
  }
  setDiaAct(nombre:number){
    this.miactividade.id_dia=nombre
  }
  setFInicioAct(nombre:number){
    this.miactividade.hora_inicio=nombre
  }
  setFFinAct(nombre:number){
    this.miactividade.hora_fin=nombre
  }
  //jdfhakjdfh
}
