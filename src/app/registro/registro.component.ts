import { Component, OnInit } from '@angular/core';
import { ServicioUsuarioService } from '../servicios/servicio-usuario.service';
import { Usuario } from '../clases/usuario';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  recontrasena:String
  miusuario:Usuario

  constructor(private serviciousuario: ServicioUsuarioService) { 
    this.miusuario=new Usuario();
    this.recontrasena=''
  }

  ngOnInit(): void {
    console.log(this.miusuario)

    
  }
  validarRegistro(){
    if(this.recontrasena=this.miusuario.password){
      console.log("Se registro: "+this.miusuario.first_name)
      this.enviarRegistro()
    }else{
      console.log("Error, las contraseñas no son iguales")
    }
  }
  

  enviarRegistro(){
    
    this.serviciousuario.addUsuarioBD(this.miusuario).subscribe(
      data => {console.log(data)
      },
      err => console.error(err),
      ()=>console.log("Se registro ")
      )
    }
}
