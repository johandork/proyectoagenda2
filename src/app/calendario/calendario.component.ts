import { Component, OnInit } from '@angular/core';
import { ServicioActividadService } from 'app/servicios/servicio-actividad.service';
import { Actividad } from '../clases/actividad'
import { Usuario } from 'app/clases/usuario';
import { ServicioUsuarioService } from 'app/servicios/servicio-usuario.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/login', title: 'Inicio de Sesion',  icon:'person', class: '' },
    { path: '/registro', title: 'Registro',  icon:'library_books', class: '' },
    { path: '/calendario', title: 'Calendario',  icon:'content_paste', class: '' },
    { path: '/actividadcrear', title: 'Nueva Actividad',  icon:'unarchive', class: '' },
    { path: '/editar', title: 'Editar Actividad', icon: 'dashboard', class: ''},
    { path: '/rendimiento', title: 'Rendimiento', icon: 'assessment', class:'' },
    { path: '/pagprincipal', title: 'Pagina Principal', icon: 'date_range', class:'' },
    { path: '/objetivos', title: 'Objetivos', icon: 'date_range', class:'' },
    { path: '/perfil', title: 'Perfil', icon: 'date_range', class:'' }
];

class tarjeta{
  estado:boolean;
  nombre:string;
}

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})


export class CalendarioComponent implements OnInit {  
  menuItems: any[];
  tarjetas:tarjeta[];
  usuario:Usuario
  username:string
  token:string
  constructor( private servicioActividad: ServicioActividadService, private serU:ServicioUsuarioService   ) {
    this.usuario=new Usuario();
    this.token=''
    this.tarjetas=[{nombre:'Hola1',estado:true},{nombre:'Hola2',estado:false}];
  }

  ngOnInit(): void {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.usuario=this.serU.getUsuario();
    this.token=this.serU.getToken();
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
        return false;
    }
    return true;
  } 
  estadoTarjeta(obj:tarjeta ){
    alert('Mi tarjeta esta en modo '+obj.estado)
  }
  agregartarjeta(){
    let nuevatarjeta:tarjeta;
    nuevatarjeta={nombre:'Hola'+(this.tarjetas.length+1),estado:false};
    this.tarjetas.push(nuevatarjeta)

  }
}


