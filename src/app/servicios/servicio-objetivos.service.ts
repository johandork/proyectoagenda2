import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { Objetivo } from 'app/clases/objetivo';
import { ServicioUsuarioService } from './servicio-usuario.service';

@Injectable({
  providedIn: 'root'
})

export class ServicioObjetivosService {
  private url = 'http://localhost:8000/aplicacion/objetivo/'
  httpHeaders = new HttpHeaders({'Content-type':'application/json'})
  
  constructor(protected http: HttpClient,private servU:ServicioUsuarioService) { }


  
  listarObjetivosget(idEstudiante:number):Observable<Objetivo[]>{
    return this.http.get<Objetivo[]>(this.url+'?id_estudiante='+1)
    //return this.http.get<Objetivo[]>(this.url+'?/id_estudiante='+idEstudiante)
  }
  obtenerUndObjetivo(objetivo:Objetivo):Observable<Objetivo[]>{
    let id=objetivo.id_objetivo;
    return this.http.get<Objetivo[]>(this.url+''+id)
  }
  actualizarObjetivo(objetivo:Objetivo):Observable<any>{
    
    let id=objetivo.id_objetivo;
    return this.http.put(this.url+id+'/',objetivo)
  }
  eliminarObjetivo(objetivo:Objetivo):Observable<any>{
    let id=objetivo.id_objetivo;
    console.log('id del objetivo',id)
    return this.http.delete(this.url+id+'/')
  }
  agregarObjetivo(objetivo:Objetivo):Observable<any>{
    return this.http.post(this.url,objetivo)
  }
}
