import { Injectable } from '@angular/core';
import { Materia } from '../clases/materia';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/catch'

@Injectable({
  providedIn: 'root'
})
export class ServicioMateriaService {

  private url = "http://localhost:8000/aplicacion/materiaestudiante/"

  constructor(protected http:HttpClient) { 
  }

  getMaterias(): Observable<any>{
    return this.http.get(this.url)
  }

  eliminarMaterias(materia:Materia):Observable<any>{
    let id=materia.id_materia;
    console.log('id de la materia',id)
    return this.http.delete(this.url+id+'/')
  }

  getlistarMaterias(idEstudiante:number):Observable<Materia[]>{
    return this.http.get<Materia[]>(this.url)
  }
 
  agregarMateria(materia:Materia):Observable<any>{
    return this.http.post(this.url,materia)
  }
}