import { Injectable } from '@angular/core';
import { Usuario } from '../clases/usuario';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { validarUsuario } from 'app/clases/validar-usuario';

@Injectable({
  providedIn: 'root'
})
export class ServicioUsuarioService {
  //validarUsuario:validarUsuario
  private miusuario: Usuario
  private token:string
  private url = "http://localhost:8000/aplicacion/users/"
  private url2 ="http://localhost:8000/aplicacion/auth/"
  constructor(protected http: HttpClient) {
    //this.validarUsuario=new validarUsuario();
    this.miusuario=new Usuario();
    this.token='';
   }

  getUsuario() {
    return this.miusuario
  }
  getToken(){
    return this.token
  }
  setUsuario(usuario: Usuario) {
    this.miusuario = usuario;
  }
  setToken(nuevoToken:string){
    this.token=nuevoToken;
  }
  addUsuarioBD(usuario: Usuario): Observable<any> {
    return this.http.post(this.url, usuario)
  }
  validarUsuarioBD(valUsu:validarUsuario):Observable<any>{
    return this.http.post(this.url2,valUsu)
  }
  getIdUsuario(username:string):Observable<Usuario[]>{
    return this.http.get<Usuario[]>('http://localhost:8000/aplicacion/users/?username='+username)
  }
}

