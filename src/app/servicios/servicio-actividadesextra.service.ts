import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/catch'
import { ActividadesExtra } from 'app/clases/actividad-extra';

@Injectable({
  providedIn: 'root'
})
export class ServicioActividadesextraService {

  private url = "http://localhost:8000/aplicacion/rutina/"

  constructor(protected http:HttpClient) { 
  }

  eliminarActividadExtra(actividadex:ActividadesExtra):Observable<any>{
    let id=actividadex.id_rutina;
    console.log('id de la actividad extra',id)
    return this.http.delete(this.url+id+'/')
  }

  getlistarActividadExtra(idEstudiante:number):Observable<ActividadesExtra[]>{
    console.log('id de la actividad extra',idEstudiante)
    return this.http.get<ActividadesExtra[]>(this.url+'?id_estudiante='+idEstudiante)
  }
  agregarActividadE(actividade:ActividadesExtra):Observable<any>{
    return this.http.post(this.url,actividade)
  }
}