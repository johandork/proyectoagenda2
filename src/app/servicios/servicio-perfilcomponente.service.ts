import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { Perfil } from 'app/clases/perfil';

@Injectable({
  providedIn: 'root'
})
export class ServicioPerfilcomponenteService {

  private url="http://localhost:8000/aplicacion/users/"

  constructor(protected http: HttpClient) { }


  putPerfil(perfil: Perfil, idEstudiante:number): Observable<Perfil>{
    return this.http.put<Perfil>(this.url+idEstudiante+"/" ,perfil)
  }

  getMostrarPerfil(idEstudiante:number):Observable<any>{
    return this.http.get(this.url )
  }
}