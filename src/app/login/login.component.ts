import { Component, OnInit } from '@angular/core';
import { validarUsuario } from '../clases/validar-usuario';
import { ServicioUsuarioService } from '../servicios/servicio-usuario.service';
import { Usuario } from '../clases/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  miU:validarUsuario
  usuario:Usuario
  miToken:string
  id:string
  nombre:string
  v:validarUsuario
  constructor(private miServ:ServicioUsuarioService) { 
    this.usuario=new Usuario()
    this.miU=new validarUsuario()
    this.id=''
    this.nombre=''
    this.miToken=''
  }

  ngOnInit(): void {
  }

  validarLogin(){

    this.miServ.validarUsuarioBD(this.miU).subscribe(
      data => {
        this.miToken=data['token'];
      },
      err => console.error(err),
      ()=>console.log('Se uso el validarUsuarioBD'))

    this.miServ.getIdUsuario(this.miU.username).subscribe(
      (data) => {
        this.usuario=data[0];

        console.log('data: ',data)
      },
      err =>console.error(err),
      ()=>{console.log('se uso el getidusuario')})
console.log('datos enviados ', this.miToken,this.usuario)
      this.miServ.setToken(this.miToken);
      this.miServ.setUsuario(this.usuario);
    }



}
