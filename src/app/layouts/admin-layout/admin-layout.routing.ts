import { Routes } from '@angular/router';
import { LoginComponent } from 'app/login/login.component';
import { PerfilComponent } from 'app/perfil/perfil.component';
import { PagprincipalComponent } from 'app/pagprincipal/pagprincipal.component';
import { RegistroComponent } from 'app/registro/registro.component';
import { ObjetivosComponent } from 'app/objetivos/objetivos.component';
import { CalendarioComponent } from 'app/calendario/calendario.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'login',          component: LoginComponent },
    { path: 'registro',       component: RegistroComponent },
    { path: 'pagprincipal', component: PagprincipalComponent},
    { path: 'perfil', component: PerfilComponent}   ,
    { path: 'objetivos', component: ObjetivosComponent},
    { path: 'perfil', component:PerfilComponent},
    { path: 'calendario',     component: CalendarioComponent },

];
