import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { ObjetivosComponent } from 'app/objetivos/objetivos.component';
import { PerfilComponent } from 'app/perfil/perfil.component';
import { PagprincipalComponent } from 'app/pagprincipal/pagprincipal.component';
import { LoginComponent } from 'app/login/login.component';
import { RegistroComponent } from 'app/registro/registro.component';
import { CalendarioComponent } from 'app/calendario/calendario.component';
import {MatMenuModule} from '@angular/material/menu'; 
import { MatIconModule } from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar'; 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule
  ],
  declarations: [
    ObjetivosComponent,
    PerfilComponent,
    PagprincipalComponent,
    LoginComponent,
    RegistroComponent,
    CalendarioComponent,

  ],
  exports:[
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatToolbarModule
  ]
})

export class AdminLayoutModule {}
